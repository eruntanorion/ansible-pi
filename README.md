Private Ansible playbooks for Raspberry Pi 2 with Fedora 29

Get this repo:
git clone git@gitlab.com:eruntanorion/ansible-pi.git

Apply changes:
git push -u origin master

Run the playbooks:
sudo ansible-pull -U https://gitlab.com/eruntanorion/ansible-pi.git

